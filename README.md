Modification of original SPNEGO-r9 library. Intead of filter, it's now standart callable library. Ie: We create instance by passing Properties as parameters and then normally use it from designed place. Configuration in current implementation is loaded from custom FormAuthenticator from file "config/spnego.properties" and value of password "spnego.preauth.password" can be standart payara server password alias (eg: ${ALIAS=somePasswordAlias}). Configuration is normal .properties file. It's required there to be parameter "enable=true", otherwise SPNEGO will be disabled.

Modified version of FormAuthenticator (public in our other repository) calls spnegoFilter.doFilter(hreq, hres) before it would return login page. SPNEGO authentication will set status code 401 (UNAUTHORIZED) (unlike the original version, flushBuffer is not called after this, so that the login page can also be attached) and FormAuthenticator attaches the login page. So even if the client does not receive a token/ticket from the AD, it can still log in using the login dialog.

Only difference in configuration is that, that instead of filter configuration in "config/default-web.xml" we will write configuration to "config/spnego.properties".



Example "config/spnego.properties" file:
```
# custom parameters
enable=false

# exclude
#spnego.exclude.dirs=
spnego.exclude.ports=4848

# autheNtication (authN) parameters
spnego.allow.basic=false
spnego.allow.localhost=false
spnego.allow.unsecure.basic=false
spnego.login.client.module=spnego-client
spnego.krb5.conf=krb5.conf
spnego.login.conf=login.conf
spnego.preauth.username=test@EXAMPLE.LOCAL
spnego.preauth.password=${ALIAS=somePasswordAlias}
spnego.login.server.module=spnego-server
spnego.prompt.ntlm=false
spnego.logger.level=5

# authoriZation (authZ) parameters
spnego.authz.class=net.sourceforge.spnego.LdapAccessControl
spnego.authz.ldap.url=ldap://ad1.example.local:389
spnego.authz.policy.file=spnego.policy
spnego.authz.unique=false
#spnego.authz.sitewide=example-role
```
