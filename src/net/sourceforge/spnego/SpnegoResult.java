package net.sourceforge.spnego;

/**
 *
 * @author Robert Zich
 */
public class SpnegoResult {
    
    public static final int RESULT_CODE_OK = 0;
    public static final int RESULT_CODE_NEGOTIATING = -1;
    public static final int RESULT_CODE_ERROR = -2;
    
    private int resultCode;
    private SpnegoPrincipal principal;
    
    public SpnegoResult() {
        this.resultCode = RESULT_CODE_OK;
        this.principal = null;
    }
    
    public SpnegoResult(int resultCode) {
        this.resultCode = resultCode;
        this.principal = null;
    }
    
    public SpnegoResult(SpnegoPrincipal principal) {
        this.resultCode = RESULT_CODE_OK;
        this.principal = principal;
    }

    public int getResultCode() {
        return resultCode;
    }
    
    public boolean isOk() {
        return resultCode == RESULT_CODE_OK;
    }
    
    public boolean isNegotiating() {
        return resultCode == RESULT_CODE_NEGOTIATING;
    }
    
    public boolean isError() {
        return resultCode == RESULT_CODE_ERROR;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
    
    public SpnegoResult withNegotiating() {
        this.resultCode = RESULT_CODE_NEGOTIATING;
        return this;
    }

    public SpnegoResult withError() {
        this.resultCode = RESULT_CODE_ERROR;
        return this;
    }
    

    public SpnegoPrincipal getPrincipal() {
        return principal;
    }

    public void setPrincipal(SpnegoPrincipal principal) {
        this.principal = principal;
    }
}
